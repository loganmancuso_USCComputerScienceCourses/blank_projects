#!/bin/bash
###
# 'server.py'
# 
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 07-05-2019--14:17:58
###

YES_OR_NO="Invalid please type [Y]es or [N]o"
SRC_DIR=".test/src"
PORT="8081"

read -p "Start Server or Kill Background Server...? [S]tart / [K]ill:    " sk_
case "${sk_}" in
	[sS]|[sS][tT][aA][rR][tT] ) # start
		echo "Starting Server "
		cd $SRC_DIR
		python3 -m http.server $PORT &
	;;
	[kK]|[kK][iI][lL][lL] ) # kill
		pkill -9 -f "python3 -m"
		echo "killing the server"
	;;
	* ) # else
		echo $YES_OR_NO | tee -a $SH_LOG
		exit 1
	;;
esac
sleep 2
ps -eaf | grep --color=auto "python3"


###
# End 'server.py'
###