# blank_projects
### Author/CopyRight: Mancuso, Logan
### Last Edit Date: 06-09-2019--15:07:15
Contains a generic folder structure of common languages

C
C++
Flex & Bison
Go
Java
JavaScript
Python

This is designed using Ubuntu (WSL v1) standards but is compatible with most Linux systems. 

This project is designed to make the development process as quick as possible. 
For each project go into the working/src folder and begin working your code into the pre-defined 
files. For simplicity I will go through the C project structure. 

open the file c > working > src > main.c

there is a section labeled as "add code here" this is where you will begin the algorithm. All stdout using echo or printf will automatically be split to stdout and written to a text file. The output can be found in iofiles > x.out. 

To build this code simply execute the file ./build and the error messages will again be sent to iofiles > x.err

then ./run will run the executable built by the makefile. 

using the iofiles/x.in will serve as an input file for the code. 

sh.log serves as a log file for the scripts that build and run the programs. 