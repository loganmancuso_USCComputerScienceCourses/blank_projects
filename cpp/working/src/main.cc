#include "main.h"

/**
 * 'main.cc'
 * Main file for program
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 08-22-2019--14:30:49
 *
**/

static const string kTag = "Main: ";

int main(int argc, char *argv[]) {

  //check arguments and store as variables
  Utils::CheckArgs(3, argc, argv, "infilename outfilename logfilename");
  in_filename = static_cast<string>(argv[1]);
  out_filename = static_cast<string>(argv[2]);
  log_filename = static_cast<string>(argv[3]);


  //open input file
  input_stream.OpenFile(in_filename);
  //start the reading

  //open log file
  Utils::LogFileOpen(log_filename);

  //open outfiles and stream
  Utils::FileOpen(out_stream, out_filename);


  //setup strings and print basic data
  log_string = kTag + "Beginning execution\n";
  log_string += kTag + Utils::TimeCall("Time Start");
  Utils::log_stream << log_string << endl;

  log_string = kTag + "infile  '" + in_filename + "'\n";
  log_string += kTag + "outfile '" + out_filename + "'\n";
  log_string += kTag + "logfile '" + log_filename + "'\n";

  Utils::log_stream << log_string << endl;
  Utils::log_stream.flush();

/**
 * Add Code Here:
**/

  out_stream << out_string;
  DoTheWork dothework;
  dothework.begin(&Utils::log_stream, &input_stream, &out_stream);


/**
 * Not below here
**/

  log_string = kTag + "Ending execution" + "\n";
  log_string += kTag + Utils::TimeCall("ending");
  Utils::log_stream << log_string << endl;
  Utils::log_stream.flush();

  Utils::FileClose(out_stream);
  Utils::FileClose(Utils::log_stream);

  // cout << kTag << "Ending execution" << endl;

  return 0;
}
/**
 * End 'main.cc'
**/